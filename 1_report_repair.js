var fs = require("fs")


function readTextFile3(fileName) {
  return fs.readFileSync(`./resources/${fileName}`).toString('utf-8')
}

let text = readTextFile3('puzzel_input_1.txt')
numberArray = text.split('\r\n')
numberArray = numberArray.map((x) =>parseInt(x));

var i
for (i=0; i <= numberArray.length; i++) {
  var j
  for (j=i; j <= numberArray.length; j++ ) {
    if (numberArray[i] + numberArray[j] === 2020) {
      console.log(numberArray[i] + ' + ' + numberArray[j] + ' = ' + (numberArray[i] + numberArray[j]))
      console.log(numberArray[i] + ' * ' + numberArray[j] + ' = ' + (numberArray[i] * numberArray[j]))
    }
  }
}